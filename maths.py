# Write a function in Python3 that parses and evaluates an arithmetic string
# Stage 1
# Support addition
# Ex. "1+2", "34+5+100", "10+20+30+40+50"
# Stage 2
# Support subtraction
# Ex. "1+2", "34-5+100", "10-20+30-40+50"
# Stage 3
# Support multiplication and division
# Ex. "1+2", "34-5*100", "10-20/30*40+50"
# You must respect the order of operations: *, / takes precedence over +, -
# You do not need to support parentheses
# You must support decimals for example "1+3/4" should return 1.75
# Do not use eval() in your function evaluate()

import re


def parse_values_and_operators(expression):
    # TODO Choose method for extracting values
    # Find all numbers in expression, supports positive numbers only
    # values = [int(x) for x in re.findall(r'\d+', expression)]
    # Find all numbers in expression, supports positive and negative numbers
    # values = [int(x) for x in re.findall(r'\-?\d+', expression)]
    values = [expression]

    # TODO Review method for extracting operations
    # Create list of all operations in expression
    operations = [i for i in expression if not i.isdigit()]

    return values, operations


def evaluate(expression):
    values, operations = parse_values_and_operators(expression)

    # TODO Implement evaluation logic here...

    return values[0]


##################################################
# DO NOT CHANGE
##################################################
def assert_same_as_eval(value):
    try:
        assert evaluate(value) == eval(value)
        print(u'\u2705', f'evaluate({value}) == eval({value})')
    except AssertionError:
        print(u'\u26d4', f'evaluate({value}) != eval({value})')


def assert_addition():
    assert_same_as_eval("1")
    assert_same_as_eval("1+2+3")
    assert_same_as_eval("1+2+3+4+5+6")
    assert_same_as_eval("1+25+312")
    pass


def assert_subtraction():
    assert_same_as_eval("-1")
    assert_same_as_eval("1+2-3")
    assert_same_as_eval("1+2-3+4+5-6")
    assert_same_as_eval("1+25-312")
    pass


def assert_multiplication_and_division():
    assert_same_as_eval("1+3/4")
    assert_same_as_eval("10-20*30+40/50")
    assert_same_as_eval("-10+20*30+40/50")
    assert_same_as_eval("-10+20*30+40/50*100/20")
    assert_same_as_eval("34-5*100")
    pass


if __name__ == "__main__":
    print("Stage 1")
    assert_addition()

    print("Stage 2")
    assert_subtraction()

    print("Stage 3")
    assert_multiplication_and_division()
