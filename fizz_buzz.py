# Write a Python program which iterates the integers from 1 to 97.
# For multiples of three print "Fizz"
# For multiples of five print "Buzz"
# For numbers which are multiples of both three and five print "FizzBuzz"
# For all other numbers print the number
# Also ensure that all assertions pass


def get_fizz_buzz(number):
    return number


if __name__ == "__main__":

    # TODO Implement Loop Here
    # for 1 to 97 call get_fizz_buzz(number)


    ##################################################
    # DO NOT CHANGE
    ##################################################
    def assert_value(number, expected):
        try:
            assert get_fizz_buzz(number) == expected
            print(u'\u2705', f'get_fizz_buzz({number}) PASSED')
        except AssertionError:
            print(u'\u26d4', f'get_fizz_buzz({number}) FAILED')


    def check_assertions():
        assert_value(3, "Fizz")
        assert_value(5, "Buzz")
        assert_value(7, 7)
        assert_value(15, "FizzBuzz")
        assert_value(30, "FizzBuzz")
        assert_value(50, "Buzz")
        assert_value(66, "Fizz")
        assert_value(330, "FizzBuzz")

    check_assertions()
